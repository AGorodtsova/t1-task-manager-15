package ru.t1.gorodtsova.tm.api.repository;

import ru.t1.gorodtsova.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
