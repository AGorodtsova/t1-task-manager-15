package ru.t1.gorodtsova.tm.api.controller;

public interface ICommandController {

    void showInfo();

    void showAbout();

    void showVersion();

    void showCommands();

    void showArguments();

    void showHelp();

}
