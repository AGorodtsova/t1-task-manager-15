package ru.t1.gorodtsova.tm.controller;

import ru.t1.gorodtsova.tm.api.controller.ICommandController;
import ru.t1.gorodtsova.tm.api.service.ICommandService;
import ru.t1.gorodtsova.tm.model.Command;
import ru.t1.gorodtsova.tm.util.FormatUtil;

public final class CommandController implements ICommandController {

    private final ICommandService commandService;

    public CommandController(final ICommandService commandService) {
        this.commandService = commandService;
    }

    @Override
    public void showInfo() {
        System.out.println("[INFO]");
        final long availableProcessors = Runtime.getRuntime().availableProcessors();
        final long freeMemory = Runtime.getRuntime().freeMemory();
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final long totalMemory = Runtime.getRuntime().totalMemory();
        final long usedMemory = totalMemory - freeMemory;

        final String freeMemoryFormat = FormatUtil.formatBytes(freeMemory);
        final String totalMemoryFormat = FormatUtil.formatBytes(totalMemory);
        final String usedMemoryFormat = FormatUtil.formatBytes(usedMemory);
        final String maxMemoryFormat = FormatUtil.formatBytes(maxMemory);
        final String maxMemoryValue = maxMemory == Long.MAX_VALUE ? "no limit" : maxMemoryFormat;

        System.out.println("Available processors: " + availableProcessors);
        System.out.println("Free memory: " + freeMemoryFormat);
        System.out.println("Maximum memory: " + maxMemoryValue);
        System.out.println("Total memory: " + totalMemoryFormat);
        System.out.println("Used memory: " + usedMemoryFormat);
    }

    @Override
    public void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("name: Anastasia Gorodtsova");
        System.out.println("email: agorodtsova@t1-consulting.ru");
    }

    @Override
    public void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.15.0");
    }

    @Override
    public void showCommands() {
        System.out.println("[COMMANDS]");
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command : commands) {
            if (command == null) continue;
            final String name = command.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

    @Override
    public void showArguments() {
        System.out.println("[ARGUMENTS]");
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command : commands) {
            if (command == null) continue;
            final String argument = command.getArgument();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument);
        }
    }

    @Override
    public void showHelp() {
        System.out.println("[HELP]");
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command : commands) System.out.println(command);
    }

}
